package com.example.noticiasupt.VIewModels;

import com.example.noticiasupt.ClassDetalle;

import java.util.List;

public class Peticion_Noticia {

    public String estado;
    public List<ClassDetalle> detalle;


    /*Peticion_Noticia(){

    }*/

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public List<ClassDetalle> getDetalle() {
        return detalle;
    }

    public void setDetalle(List<ClassDetalle> detalle) {
        this.detalle = detalle;
    }

}
