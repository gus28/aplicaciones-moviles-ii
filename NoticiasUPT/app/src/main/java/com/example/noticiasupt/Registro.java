package com.example.noticiasupt;


import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.noticiasupt.VIewModels.Registro_Usuario;
import com.example.noticiasupt.api.Api;
import com.example.noticiasupt.api.Servicios.ServicioPeticion;

import okhttp3.Call;
import okhttp3.Response;
import retrofit2.Callback;

public class Registro extends AppCompatActivity {

    private TextView regresar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        regresar = (TextView) findViewById(R.id.tv_inicio);

        regresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(Registro.this, MainActivity.class);
                startActivity(intent);
            }
        });

        Button btnGuardar = (Button) findViewById(R.id.btn_regis);
        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                EditText nombreUsuario = (EditText) findViewById(R.id.usernameRegistro);
                EditText password1 = (EditText) findViewById(R.id.password1Registro);
                EditText password2 = (EditText) findViewById(R.id.password2Registro);

                if (nombreUsuario.getText().toString().isEmpty() || nombreUsuario.getText().toString() == ""){
                    nombreUsuario.setSelectAllOnFocus(true);
                    nombreUsuario.requestFocus();
                    nombreUsuario.setError("Inserta un usuario");
                    return;
                }

                if (password1.getText().toString().isEmpty() || password1.getText().toString() == ""){
                    password1.setSelectAllOnFocus(true);
                    password1.requestFocus();
                    password1.setError("Inserta una contraseña");
                    return;
                }

                if (password2.getText().toString().isEmpty() || password2.getText().toString() == ""){
                    password2.setSelectAllOnFocus(true);
                    password2.requestFocus();
                    password2.setError("Inserta una contraseña");
                    return;
                }

                if (!password1.getText().toString().equals(password2.getText().toString())){
                    Toast.makeText(Registro.this, "Las contraseñas no coinciden", Toast.LENGTH_LONG).show();
                }

                ServicioPeticion service = Api.getApi(Registro.this).create(ServicioPeticion.class);
                retrofit2.Call<Registro_Usuario> registrarCall = service.registarUsuario(nombreUsuario.getText().toString(),password1.getText().toString());
                registrarCall.enqueue(new Callback<Registro_Usuario>() {
                    @Override
                    public void onResponse(retrofit2.Call<Registro_Usuario> call, retrofit2.Response<Registro_Usuario> response) {
                        Registro_Usuario peticion = response.body();
                        if (response.body() == null){
                            Toast.makeText(Registro.this,"Ocurrio un error, intentalo mas tarde", Toast.LENGTH_LONG).show();
                            return;
                        }
                        if(peticion.estado == "true"){
                            startActivity(new Intent(Registro.this,MainActivity.class));
                            Toast.makeText(Registro.this, "Datos registrados", Toast.LENGTH_LONG).show();
                        }else{
                            Toast.makeText(Registro.this, peticion.detalle, Toast.LENGTH_LONG).show();
                        }

                    }

                    @Override
                    public void onFailure(retrofit2.Call<Registro_Usuario> call, Throwable t) {
                        Toast.makeText(Registro.this,"Error",Toast.LENGTH_LONG).show();

                    }
                });

            }
        });
    }
}
