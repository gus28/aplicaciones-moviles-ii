package com.example.noticiasupt.VIewModels;

public class Peticion_Login {

    private String correo;
    private String password;
    public String token;
    public String estado;

    public Peticion_Login(){

    }

    Peticion_Login(String correo, String password){
        this.correo = correo;
        this.password = password;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
}
