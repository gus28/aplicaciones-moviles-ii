package com.example.noticiasupt.api.Servicios;

import com.example.noticiasupt.VIewModels.Peticion_Login;
import com.example.noticiasupt.VIewModels.Peticion_Noticia;
import com.example.noticiasupt.VIewModels.Registro_Notificacion;
import com.example.noticiasupt.VIewModels.Registro_Usuario;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface ServicioPeticion {

    @FormUrlEncoded
    @POST("api/crearUsuario")
    Call<Registro_Usuario> registarUsuario(@Field("username") String correo, @Field("password") String contrasenia);

    @FormUrlEncoded
    @POST("api/login")
    Call<Peticion_Login> getLogin(@Field("username") String correo, @Field("password") String contrasenia);

    @GET("api/todasNot")
    Call<Peticion_Noticia> getNoticias();

    @FormUrlEncoded
    @POST("api/crearNotUsuario")
    Call<Registro_Notificacion> registarNotificacion(@Field("titulo") String titulo, @Field("descripcion") String descripcion, @Field("usuarioId") String usuarioId);

}
