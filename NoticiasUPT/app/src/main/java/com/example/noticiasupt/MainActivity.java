package com.example.noticiasupt;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.noticiasupt.VIewModels.Peticion_Login;
import com.example.noticiasupt.api.Api;
import com.example.noticiasupt.api.Servicios.ServicioPeticion;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    private Button iniciar;

    private TextView crear;
    private String APITOKEN = "";

    private EditText correoet;
    private EditText passwordet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        iniciar = (Button) findViewById(R.id.btn_inicio);
        crear = (TextView) findViewById(R.id.tv_crear);

        Verificar();

        iniciar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                correoet = (EditText)findViewById(R.id.etEmail);
                passwordet = (EditText) findViewById(R.id.etPass);

                if (correoet.getText().toString().isEmpty() || correoet.getText().toString() == ""){
                    correoet.setSelectAllOnFocus(true);
                    correoet.requestFocus();
                    correoet.setError("Inserta un correo");
                    return;
                }

                if (passwordet.getText().toString().isEmpty() || passwordet.getText().toString() == ""){
                    passwordet.setSelectAllOnFocus(true);
                    passwordet.requestFocus();
                    passwordet.setError("Inserta una contraseña");
                    return;
                }

                realizarlogin();

                /*ServicioPeticion service = Api.getApi(MainActivity.this).create(ServicioPeticion.class);
                retrofit2.Call<Peticion_Login> loginCall = service.getLogin(correoet.getText().toString(),passwordet.getText().toString());
                loginCall.enqueue(new Callback<Peticion_Login>() {
                    @Override
                    public void onResponse(retrofit2.Call<Peticion_Login> call, Response<Peticion_Login> response) {
                        Peticion_Login peticion = response.body();
                        if(peticion.estado == "true"){

                            APITOKEN = peticion.token;

                            guardarPreferencias();

                            Toast.makeText(MainActivity.this, "Bienvenido", Toast.LENGTH_LONG).show();


                            startActivity(new Intent(MainActivity.this, Inicio.class));
                        }else {
                            Toast.makeText(MainActivity.this, "Datos incorrectos", Toast.LENGTH_LONG).show();
                        }

                    }

                    @Override
                    public void onFailure(retrofit2.Call<Peticion_Login> call, Throwable t) {
                        Toast.makeText(MainActivity.this,"Error",Toast.LENGTH_LONG).show();

                    }
                });*/

            }
        });

        crear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent(MainActivity.this, Registro.class);
                startActivity(intent1);
            }
        });
    }

    public void realizarlogin(){

        ServicioPeticion service = Api.getApi(MainActivity.this).create(ServicioPeticion.class);
        retrofit2.Call<Peticion_Login> loginCall = service.getLogin(correoet.getText().toString(),passwordet.getText().toString());
        loginCall.enqueue(new Callback<Peticion_Login>() {
            @Override
            public void onResponse(retrofit2.Call<Peticion_Login> call, Response<Peticion_Login> response) {
                Peticion_Login peticion = response.body();
                if(peticion.estado == "true"){

                    APITOKEN = peticion.token;

                    guardarPreferencias();

                    Toast.makeText(MainActivity.this, "Bienvenido", Toast.LENGTH_LONG).show();


                    startActivity(new Intent(MainActivity.this, Inicio.class));
                }else {
                    Toast.makeText(MainActivity.this, "Datos incorrectos", Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onFailure(retrofit2.Call<Peticion_Login> call, Throwable t) {
                Toast.makeText(MainActivity.this,"Error1",Toast.LENGTH_LONG).show();

            }
        });

    }

    public void guardarPreferencias (){
        SharedPreferences preferencias = getSharedPreferences("credenciales", Context.MODE_PRIVATE);
        String token = APITOKEN;
        SharedPreferences.Editor editor = preferencias.edit();
        editor.putString("TOKEN", token);
        editor.commit();
    }

    public void Verificar(){
        SharedPreferences preferencias = getSharedPreferences("credenciales", Context.MODE_PRIVATE);
        String token = preferencias.getString("TOKEN","");
        if(token != ""){
            Toast.makeText(MainActivity.this,"Bienvenido Nuevamente", Toast.LENGTH_LONG).show();
            startActivity(new Intent(MainActivity.this,Inicio.class));
        }

    }
}
