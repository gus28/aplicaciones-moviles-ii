package com.example.noticiasupt;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.noticiasupt.VIewModels.Peticion_Noticia;
import com.example.noticiasupt.VIewModels.Registro_Notificacion;
import com.example.noticiasupt.api.Api;
import com.example.noticiasupt.api.Servicios.ServicioPeticion;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Callback;
import retrofit2.Response;

public class Inicio extends AppCompatActivity {

    ArrayList<String> titles = new ArrayList<>();

    private Button notificacion;
    private EditText usuarioid, tituloet, descripcionet;
    private TextView cerrar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inicio);

        cerrar = (TextView)findViewById(R.id.tv_cerrar);
        /*cerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences preferencias = getSharedPreferences("credenciales", Context.MODE_PRIVATE);
                String token = "";
                SharedPreferences.Editor editor = preferencias.edit();
                editor.putString("TOKEN", token);
                editor.commit();

                Intent intent1 = new Intent(Inicio.this, MainActivity.class);
                startActivity(intent1);
            }
        });*/

        //MostarNoticia();

        notificacion = (Button)findViewById(R.id.btn_notif);

        notificacion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                usuarioid = (EditText)findViewById(R.id.et_usuarId);
                tituloet = (EditText)findViewById(R.id.et_titulo);
                descripcionet = (EditText)findViewById(R.id.et_descrip);

                if (usuarioid.getText().toString().isEmpty() || usuarioid.getText().toString() == ""){
                    usuarioid.setSelectAllOnFocus(true);
                    usuarioid.requestFocus();
                    usuarioid.setError("Inserta un id");
                    return;
                }

                if (tituloet.getText().toString().isEmpty() || tituloet.getText().toString() == ""){
                    tituloet.setSelectAllOnFocus(true);
                    tituloet.requestFocus();
                    tituloet.setError("Inserta un titulo");
                    return;
                }

                if (descripcionet.getText().toString().isEmpty() || descripcionet.getText().toString() == ""){
                    descripcionet.setSelectAllOnFocus(true);
                    descripcionet.requestFocus();
                    descripcionet.setError("Inserta una descripcion");
                    return;
                }

                //R_Notificacion();

            }
        });
    }

    /*public void MostarNoticia(){

        final ArrayAdapter arrayAdapter = new ArrayAdapter(this,android.R.layout.simple_list_item_1,titles);

        ServicioPeticion service = Api.getApi(Inicio.this).create(ServicioPeticion.class);
        retrofit2.Call<Peticion_Noticia> NoticiaCall = service.getNoticias();
        NoticiaCall.enqueue(new Callback<Peticion_Noticia>() {
            @Override
            public void onResponse(retrofit2.Call<Peticion_Noticia> call, Response<Peticion_Noticia> response) {
                Peticion_Noticia peticion = response.body();

                if(peticion.estado == "true"){

                    List<ClassDetalle> detalles = peticion.detalle;

                    for (ClassDetalle mostrar : detalles){

                        titles.add(mostrar.getId());
                        titles.add(mostrar.getTitulo());
                        titles.add(mostrar.getDescripcion());
                    }

                    Toast.makeText(Inicio.this, ""+titles, Toast.LENGTH_LONG).show();

                }else {
                    Toast.makeText(Inicio.this, "Datos incorrectos", Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onFailure(retrofit2.Call<Peticion_Noticia> call, Throwable t) {
                Toast.makeText(Inicio.this,"Error2",Toast.LENGTH_LONG).show();

            }
        });

    }*/

    /*public void R_Notificacion(){

        ServicioPeticion service = Api.getApi(Inicio.this).create(ServicioPeticion.class);
        retrofit2.Call<Registro_Notificacion> registrarCall = service.registarNotificacion(usuarioid.getText().toString(),tituloet.getText().toString(),descripcionet.getText().toString());
        registrarCall.enqueue(new Callback<Registro_Notificacion>() {
            @Override
            public void onResponse(retrofit2.Call<Registro_Notificacion> call, retrofit2.Response<Registro_Notificacion> response) {
                Registro_Notificacion peticion = response.body();
                if (response.body() == null){
                    Toast.makeText(Inicio.this,"Ocurrio un error, intentalo mas tarde", Toast.LENGTH_LONG).show();
                    return;
                }
                if(peticion.estado == "true"){

                    Toast.makeText(Inicio.this, "Datos registrados", Toast.LENGTH_LONG).show();
                }else{
                    Toast.makeText(Inicio.this, peticion.detalle, Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onFailure(retrofit2.Call<Registro_Notificacion> call, Throwable t) {
                Toast.makeText(Inicio.this,"Errorinicio",Toast.LENGTH_LONG).show();

            }
        });

    }*/
}
